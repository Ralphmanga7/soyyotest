//
//  DateExtensions.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    var stringFormat : String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    func adding(days:Int) -> Date? {
         return Calendar.current.date(byAdding: .day, value: days, to: self)
    }
}
