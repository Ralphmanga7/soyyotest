//
//  UITableViewExtensions.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit

extension UITableView {
    func animatedReaload() {
        
        self.reloadData()
        let tableViewHeight = self.bounds.size.height
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
            UIView.animate(withDuration: 0.8, delay: 0.06 * Double(delayCounter), usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}
