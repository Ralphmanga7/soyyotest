//
//  ViewController.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var customDateField : UITextField!
    @IBOutlet weak var dateContainer : UIView!
    private let apodViewModel = APODViewModel()
    let transition = Animator()
    var selectedCell : ApodCell!
    var visibleIndexPath: IndexPath? = nil
    
    var apods : [APOD]! = [] {
        didSet{
            tableView.animatedReaload()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configBindings()
        setupView()
        apodViewModel.loadAPODs()
    }

    func setupView(){
        let nib = UINib.init(nibName: "ApodCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ApodCell")
        dateContainer.layer.shadowColor = UIColor.black.cgColor
        dateContainer.layer.shadowRadius = 12
        dateContainer.layer.shadowOpacity = 0.18
        customDateField.delegate = self
    }
    
    func configBindings(){
        apodViewModel.onDidLoadAPODs = {
            self.apods = self.apodViewModel.apods
        }
        apodViewModel.onDidLoadAPOD = {
            if let result = self.apodViewModel.apod{
                self.apods = [result]
            }
        }
        apodViewModel.onDidFailLoad = { error in
            print(error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"{
            let detail = segue.destination as! DetailController
            detail.apod = selectedCell.apod
            detail.transitioningDelegate = self
        }
    }

}

extension ViewController :  UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell = tableView.cellForRow(at: indexPath) as? ApodCell
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "detail", sender: self)
    }
}

extension ViewController :  UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let visibleIndexPath = self.visibleIndexPath {
            if indexPath.row > visibleIndexPath.row {
                (cell as! ApodCell).animate(height: tableView.bounds.height)
            }
        }else{
            visibleIndexPath = indexPath
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 266
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return apods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApodCell", for: indexPath) as! ApodCell
        cell.apod = apods[indexPath.row]
        return cell
    }
    
}

extension ViewController : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()

        visibleRect.origin = tableView.contentOffset
        visibleRect.size = tableView.bounds.size

        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)

        if let visibleIndexPath = tableView.indexPathForRow(at: visiblePoint){
            self.visibleIndexPath = visibleIndexPath
        }
    }
}

extension ViewController : UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.pictureFrame = selectedCell.container.convert(selectedCell.picture.frame, to: view)
        return transition
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}

extension ViewController : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField.text!.isEmpty){
            if apods.count == 1 {
                apods = []
                apodViewModel.loadAPODs()
            }
        }else{
            apodViewModel.loadAPOD(date: textField.text!)
        }
    }
}
