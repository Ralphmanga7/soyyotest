//
//  DetailController.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit
import WebKit

class DetailController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var seeButton: UIButton!
    
    let spaccing = CGFloat(20)
    var apod : APOD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        fillData()
        name.sizeToFit()
        content.frame.origin.y = name.frame.maxY + spaccing
        content.sizeToFit()
        scrollView.contentSize = CGSize(width: 0, height: content.frame.maxY + spaccing)
    }
    
    func fillData(){
        guard apod != nil else {return}
        self.name.text = apod.title
        self.date.text = apod.date
        self.content.text = apod.explanation
        author.text = apod.copyright != nil ? "© \(apod.copyright!)" : "© NASA"
        if apod.media_type == "video"{
            webView.isHidden = false
            picture.isHidden = true
            seeButton.isHidden = true
            bottomView.isHidden = true
            scrollView.frame.size.height = bottomView.frame.minY
            loadWebView(url: apod.url)
        }else{
            picture.downloaded(from: apod.url)
            picture.contentMode = .scaleAspectFill
            webView.isHidden = true
            picture.isHidden = false
            seeButton.isHidden = false
        }
    }
    
    @IBAction func seeOnTrueSize(){
        self.performSegue(withIdentifier: "image", sender: self)
    }
    
    @IBAction func close(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadWebView(url:String){
        if let requestUrl = URL(string:url){
            let request = URLRequest(url: requestUrl)
            webView.load(request)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "image"{
            let image = segue.destination as! ImageController
            image.apod = apod
        }
    }
}
