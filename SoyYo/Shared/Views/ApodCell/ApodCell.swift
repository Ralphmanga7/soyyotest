//
//  ApodCell.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit
import WebKit

class ApodCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var container: UIView!
    var apod : APOD!{
        didSet{
            guard apod != nil else {return}
            self.name.text = apod.title
            self.date.text = apod.date
            author.text = apod.copyright != nil ? "© \(apod.copyright!)" : "© NASA"
            if apod.media_type == "video"{
                webView.isHidden = false
                picture.isHidden = true
                loadWebView(url: apod.url)
            }else{
                picture.downloaded(from: apod.url)
                picture.contentMode = .scaleAspectFit
                webView.isHidden = true
                picture.isHidden = false
            }
        }
    }
    
    func loadWebView(url:String){
        if let requestUrl = URL(string:url){
            let request = URLRequest(url: requestUrl)
            webView.load(request)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.container.layer.cornerRadius = 12
        self.container.layer.shadowColor = UIColor.black.cgColor
        self.container.layer.shadowRadius = 12
        self.container.layer.shadowOpacity = 0.08
        self.container.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.picture.cornerRadius(radius: 12)
        self.webView.layer.cornerRadius = 12
        self.webView.layer.masksToBounds = true
        self.picture.contentMode = .scaleAspectFill
        self.selectionStyle = .none
    }
    
    func animate(height: CGFloat){
        self.transform = CGAffineTransform(translationX: 0, y: height)
        UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.picture.image = UIImage.init(named: "placeholderImage")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
