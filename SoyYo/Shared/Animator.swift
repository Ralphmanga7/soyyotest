//
//  Animator.swift
//  SoyYo
//
//  Created by RalphM on 13/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit

class Animator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration: TimeInterval = 1.2
    var presenting = true
    var originFrame = CGRect.zero
    var pictureFrame = CGRect.zero
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        containerView.addSubview(toView)
        originFrame = toView.frame
        toView.frame = pictureFrame
        toView.layer.cornerRadius = 12
        toView.layer.masksToBounds = true
        toView.subviews.forEach{
            if $0.tag != 7 {
                $0.alpha = 0
            }
        }
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            toView.frame = self.originFrame
            
        }, completion: { _ in
            UIView.animate(withDuration: 0.1) {
                toView.subviews.forEach{
                    if $0.tag != 7 {
                        $0.alpha = 1
                    }
                }
                toView.layer.cornerRadius = 0
            }
            transitionContext.completeTransition(true)
        })
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
      return duration
    }
}
