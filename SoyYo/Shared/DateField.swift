//
//  DateField.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import UIKit

@IBDesignable
class DateField: UITextField {
    let padding = CGFloat(20)
    let datePicker = UIDatePicker()
    let toolbar = UIToolbar();
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = 12
        self.layer.masksToBounds = true
    }
    
    func updateView() {
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(done))
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        datePicker.minimumDate = dateFormatter.date(from: "1995-06-20")
        toolbar.sizeToFit()
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        self.inputAccessoryView = toolbar
        self.inputView = datePicker
        if let image = leftImage {
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            rightView = imageView
        }
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= padding
        return textRect
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + padding, y: bounds.origin.y, width: bounds.width - (padding*2), height: bounds.height)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding, dy: 0)
    }
    
    @objc func done(){
        self.text = datePicker.date.stringFormat
        self.resignFirstResponder()
    }
    
    @objc func cancel(){
        self.text = ""
        self.resignFirstResponder()
    }
    
    
    
}
