//
//  APODViewModel.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import Foundation

class APODViewModel {
    
    private(set) var apods: [APOD]? = []
    private(set) var apod: APOD?
    
    var onDidLoadAPODs: (() -> Void)?
    var onDidLoadAPOD: (() -> Void)?
    var onDidFailLoad : ((_ error:String) -> Void)?
    
    func loadAPODs() {
        API.retrieveAPODs { [weak self] (apods) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.apods = apods?.reversed()
            strongSelf.onDidLoadAPODs?()
        }
    }
    
    func loadAPOD(date: String) {
        API.retrieveAPODForDate(date: date) { [weak self] (apod) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.apod = apod
            strongSelf.onDidLoadAPOD?()
        }
    }
    
}
