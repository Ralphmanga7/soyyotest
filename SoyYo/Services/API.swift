//
//  API.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import Foundation
import Alamofire

typealias APODsCallback = ([APOD]?) -> ()
typealias APODCallback = (APOD?) -> ()

protocol APIProtocol {
    static func retrieveAPODs(completionHandler: @escaping (APODsCallback))
    static func retrieveAPODForDate(date: String, completionHandler: @escaping (APODCallback))
}

struct API {
    static private let baseURL = "https://api.nasa.gov/planetary/apod"
    static private let apiKey = "fqJfFe429JOgvwElhY0YSLtnLzOiZD5xQgpPUpqF"
    static private func getHeaders() -> HTTPHeaders {
        return ["content-type": "application/json"]
    }
    static private func apiURL() -> String{
        return "\(baseURL)?api_key=\(apiKey)&"
    }
}

extension API : APIProtocol {
    /// Fetch APODs
    static func retrieveAPODs(completionHandler: @escaping (APODsCallback)){
        let endDate = Date().stringFormat
        let startDate = Date().adding(days: -20)?.stringFormat ?? ""
        let url = apiURL() + "start_date=\(startDate)&end_date=\(endDate)"
        AF.request(url, method: .get, headers:getHeaders()).responseJSON { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let apods = try? JSONDecoder().decode([APOD].self, from: response.data!) else { break }
                        completionHandler(apods)
                    break
                    case .failure(let error):
                        print(error.errorDescription!)
                        completionHandler(nil)
                    break
                }
            }
        }
    }
    
    /// Fetch APODfromDate
    static func retrieveAPODForDate(date: String, completionHandler: @escaping (APODCallback)){
        let url = apiURL() + "date=\(date)"
        AF.request(url, method: .get, headers:getHeaders()).responseJSON { response in
            DispatchQueue.main.async {
                switch response.result {
                    case .success:
                        guard let apod = try? JSONDecoder().decode(APOD.self, from: response.data!) else { break }
                        completionHandler(apod)
                    break
                    case .failure(let error):
                        print(error.errorDescription!)
                        completionHandler(nil)
                    break
                }
            }
        }
    }
}
