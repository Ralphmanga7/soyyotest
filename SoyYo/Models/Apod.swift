//
//  APOD.swift
//  SoyYo
//
//  Created by RalphM on 12/09/20.
//  Copyright © 2020 Ralph. All rights reserved.
//

import Foundation

struct APOD : Codable {
    let title : String
    let explanation : String
    let date : String
    let media_type : String
    let service_version : String
    let url : String
    let copyright : String?
    let hdurl : String?

    enum mediaType : String {
        case video = "video"
        case image = "image"
    }
}
